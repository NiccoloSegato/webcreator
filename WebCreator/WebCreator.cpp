// WebCreator
//
// Copyright 2017 - D'Elia Lorenzo e Segato Niccolo
//

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <Windows.h>
#include <string>

#define KEY_ENTER 0x0D
#define KEY_ESCAPE 0x1B
using namespace std;

ofstream file;

void aprifile() {
	file.open("index.html", ios::out);
}

class Header {
public:
	void esegui() {
		cout << "> HEADER > ";
		string comando;
		cin >> comando;
		smistamento(comando);
	}

	void smistamento(string comando) {
		if (comando == "image") {
			image();
		}
		else if (comando == "title") {
			title();
		}
		else if (comando == "exit") {
			exit();
		}
		else {
			error(comando);
		}
	}

	void image() {
		cout << "Inserire l'URL della pagina: ";
		string url;
		cin >> url;
		cout << url;
		Sleep(2000);
		file << "<img src=\"" + url + "\" alt=\"Mountain View\" style=\"width:304px; height:228px; \">";
		esegui();
	}

	void title() {
		cout << "Inserire il titolo della pagina: ";
		string titoloPagina;
		cin >> titoloPagina;
		string html = "<HTML><HEAD><TITLE>" + titoloPagina + "</TITLE>";
		file << html;
		esegui();
	}

	void exit() {
		file << "</HEAD>";
	}

	void error(string comando) {
		cout << "Il comando \"" << comando << "\" non corrisponde a nessun comando." << endl << "Digita \"help\"per la lista dei comandi disponibili" << endl << endl;
		esegui();
	}
};

class PaginaWeb {
private:
	Header headerClass;
public:
	void creaPaginaWeb() {
		cout << "WEB PAGE CREATOR" << endl << "Created by Lorenzo D'Elia e Niccolo Segato" << endl << endl;
		cout << "Apertura file..." << endl;
		Sleep(1000);
		cout << endl << "Premere INVIO per continuare...";
		while (true) {
			if (GetAsyncKeyState(KEY_ENTER)) {
				break;
			}
		}
		system("cls");
		esegui(true);
	}

	void chiudiPagina() {
		file.close();
	}

	void esegui(bool first) {
		string comando;
		if (first) {
			system("cls");
		}
		cout << "> ";
		getline(cin, comando);
		Sleep(300);
		smistamento(comando);
	}

	void smistamento(string comando) {
		if (comando == "help") {
			help();
		}
		else if (comando == "clear") {
			clear();
		}
		else if (comando == "header") {
			header();
		}
		/*else if (comando == "open browser") {
			open_browser();
		}*/
		else if (comando == "exit") {
			exit();
		}
		else {
			error(comando);
		}
	}

	void help() {
		cout << endl << "WEB PAGE CREATOR" << endl << "Created by Lorenzo D'Elia & Niccolo Segato" << endl << endl;
		cout << "help" << "     Fornisce un elenco dei comandi disponibili" << endl;
		cout << "clear" << "    Pulisce il terminale dalle scritte" << endl;
		cout << "header" << "   Entra nell'editor dell'header" << endl;
		cout << "exit" << "     Chiude e salva il file" << endl;
		cout << endl;
		esegui(false);
	}

	void clear() {
		system("cls");
		esegui(false);
	}

	void error(string errore) {
		cout << "Il comando \"" << errore << "\" non corrisponde a nessun comando." << endl << "Digita \"help\"per la lista dei comandi disponibili" << endl << endl;
		esegui(false);
	}

	void header() {
		if (GetAsyncKeyState(KEY_ESCAPE)) {
			esegui(false);
		}
		else {
			headerClass.esegui();
			esegui(false);
		}
	}

	/*void open_browser() {
		ShellExecute(NULL, "open", "C:\Users\segat\Documents\Visual Studio 2017\Projects\WebCreator\WebCreator\index.html", NULL, NULL, SW_SHOWNORMAL);
	}*/

	void exit() {
		string end = "</HTML>";
		file << end;
		chiudiPagina();
	}
};




int main()
{
	PaginaWeb web;
	aprifile();
	web.creaPaginaWeb();
	system("pause");
    return 0;
}

